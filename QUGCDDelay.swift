//
//  QUGCDDelay.swift
//  Quander
//
//  Created by Sean Howard on 12/06/2016.
//  Copyright © 2016 Quander Ltd. All rights reserved.
//

import Foundation

typealias dispatch_cancelable_closure = (_ cancel : Bool) -> Void

func delay(time:TimeInterval, closure:@escaping ()->Void) ->  dispatch_cancelable_closure? {
    
    func dispatch_later(clsr:@escaping ()->Void) {
        
        let deadlineTime = DispatchTime.now() + .seconds(1)
        
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) { 
            clsr()
        }
        
    }
    
    var closure:((Void) -> Void)? = closure
    var cancelableClosure:dispatch_cancelable_closure?
    
    let delayedClosure:dispatch_cancelable_closure = { cancel in
        if closure != nil {
            if (cancel == false) {
                DispatchQueue.main.async(execute: closure!);
            }
        }
        closure = nil
        cancelableClosure = nil
    }
    
    cancelableClosure = delayedClosure
    
    dispatch_later {
        if let delayedClosure = cancelableClosure {
            delayedClosure(false)
        }
    }
    
    return cancelableClosure;
}

func cancel_delay(closure:dispatch_cancelable_closure?) {
    
    if closure != nil {
        closure!(true)
    }
}

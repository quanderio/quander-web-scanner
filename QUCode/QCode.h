//
//  QCode.h
//  Studio Remote
//
//  Created by Gavin Williams on 16/07/2016.
//  Copyright © 2016 Sean Howard. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QAttendeeCode.h"

@interface QCode : NSObject

typedef NS_ENUM(NSUInteger, CodeEncodingType) {
    kQCodeEncodingTypeURL,
    kQCodeEncodingTypeEncrypted,
    kQCodeEncodingTypeJSON,
    kQCodeEncodingTypeUnknown
};

+ (QAttendeeCode *) codeFromString: (NSString *) string;
+ (CodeEncodingType) determineCodeType:(NSString *) code;

@end

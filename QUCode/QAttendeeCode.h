//
//  QAttendeeCode.h
//  Studio Remote
//
//  Created by Gavin Williams on 17/07/2016.
//  Copyright © 2016 Sean Howard. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const kQAttendeeCodeTypeReferenceId;
FOUNDATION_EXPORT NSString *const kQAttendeeCodeTypeAttendeeUuid;

@interface QAttendeeCode : NSObject

@property (nonatomic, copy) NSString *code;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *firstname;
@property (nonatomic, copy) NSString *lastname;

- (NSDictionary *) serialize;

@end

//
//  QCode.m
//  Studio Remote
//
//  Created by Gavin Williams on 16/07/2016.
//  Copyright © 2016 Sean Howard. All rights reserved.
//

#import "QCode.h"
#import "RSA.h"

@implementation QCode


NSString *rsaPublicKey = @"-----BEGIN PUBLIC KEY-----MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKkEvmh69XsykSgEGGE354aDEJwuQAAfG9obSp88PrCC+O3kxYGqSbDae3+ufCMSJRBGqzifOyHljmZf7OdTL1kCAwEAAQ==-----END PUBLIC KEY-----";

NSString *rsaPrivateKey = @"-----BEGIN RSA PRIVATE KEY-----MIIBVAIBADANBgkqhkiG9w0BAQEFAASCAT4wggE6AgEAAkEAqQS+aHr1ezKRKAQYYTfnhoMQnC5AAB8b2htKnzw+sIL47eTFgapJsNp7f658IxIlEEarOJ87IeWOZl/s51MvWQIDAQABAkBW8fLFKmN3YZbcP+cOs8RtJKT5wqz3owkf1KQ5b7NL9uyVvgRF/NMewm09qS3UBSkMmPOC+nwD83UEhEtj2ECBAiEA6PxlisxxG+aMNKLdIauovkqCfcH4Ppfxg502pPq/VJECIQC5tseG6pULWWit76tWKM+3Q/xSu6Os/lnB50o46SPySQIgdAEPsf8/JiwxjRe2UMh+uViyBlmo98mBqA2EIrryvvECIQCZ/kTuy7+w/H9/kzfIpuiud3JYC/2JqhM1ZRs3m6LR+QIge4zNJ81ddLqlna9VJRvNbz5WOw76JxY1+TLT8j88HzA=-----END RSA PRIVATE KEY-----";

+ (QAttendeeCode *) codeFromString:(NSString *)codeString {
    
    codeString = [codeString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    // Determine the code type...
    switch ([self determineCodeType:codeString]){
        case kQCodeEncodingTypeURL:
            return [self codeFromURL:[NSURL URLWithString:codeString]];
            break;
        case kQCodeEncodingTypeJSON:
            return [self codeFromJSONString:codeString];
            break;
        case kQCodeEncodingTypeEncrypted:
            return [self codeFromEncryptedString:codeString];
            break;
        case kQCodeEncodingTypeUnknown:
            return nil;
            break;
    }
    
}

+ (QAttendeeCode *) codeFromURL:(NSURL *) dirtyCode {
    
    QAttendeeCode *attendeeCode = [[QAttendeeCode alloc] init];
    attendeeCode.type = kQAttendeeCodeTypeReferenceId;
    attendeeCode.code = [dirtyCode lastPathComponent];
    
    return attendeeCode;
    
}

+ (QAttendeeCode *) codeFromJSONString:(NSString *) dirtyCode {
    
    NSError *error;
    
    
    id object = [NSJSONSerialization JSONObjectWithData:[dirtyCode dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error];
    
    if(error){
        return nil;
    }
    
    if (![object isKindOfClass:[NSDictionary class]]){
        return nil;
    }
    
    NSDictionary *codeDictionary = (NSDictionary *) object;
    
    QAttendeeCode *attendeeCode = [[QAttendeeCode alloc] init];
    
    if(codeDictionary[@"referenceId"] && ((NSString *) codeDictionary[@"referenceId"]).length > 0){
        
        attendeeCode.type = kQAttendeeCodeTypeReferenceId;
        attendeeCode.code = codeDictionary[@"referenceId"];
        
        return attendeeCode;
        
    }
    
    if(codeDictionary[@"attendeeUuid"] && ((NSString *) codeDictionary[@"attendeeUuid"]).length > 0) {
        
        attendeeCode.type = kQAttendeeCodeTypeAttendeeUuid;
        attendeeCode.code = codeDictionary[@"attendeeUuid"];
        
        if(codeDictionary[@"firstname"]){
            attendeeCode.firstname = codeDictionary[@"firstname"];
        }
        
        if(codeDictionary[@"lastname"]){
            attendeeCode.lastname = codeDictionary[@"lastname"];
        }
        
        return attendeeCode;
        
    }
    
    
    return nil;
}

+ (QAttendeeCode *) codeFromEncryptedString:(NSString *) dirtyCode {
    
    // Decrypt
    NSString *dirtyString = [RSA decryptString:dirtyCode privateKey:rsaPrivateKey];
    
    if(dirtyString == nil){
        return nil;
    }
    
    // Call codeFromJSONString
    return [self codeFromJSONString:dirtyString];
}

+ (CodeEncodingType) determineCodeType:(NSString *) code {
    
    CodeEncodingType type = kQCodeEncodingTypeUnknown;
    
    // Try to decrypt it
    NSString *decryptedString = [RSA decryptString:code privateKey:rsaPrivateKey];
    if(decryptedString && decryptedString.length > 0){
        return kQCodeEncodingTypeEncrypted;
    }
    
    // Try to deserialize it
    NSError *error = nil;
    [NSJSONSerialization JSONObjectWithData:[code dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error];
    if (!error){
        return kQCodeEncodingTypeJSON;
    }
    
    // Check to see if it's a URL (old code)
    if ([NSURL URLWithString:code]){
        return kQCodeEncodingTypeURL;
    }
    
    return type;
    
}

@end

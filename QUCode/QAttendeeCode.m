//
//  QAttendeeCode.m
//  Studio Remote
//
//  Created by Gavin Williams on 17/07/2016.
//  Copyright © 2016 Sean Howard. All rights reserved.
//

#import "QAttendeeCode.h"

NSString *const kQAttendeeCodeTypeReferenceId = @"referenceId";
NSString *const kQAttendeeCodeTypeAttendeeUuid = @"attendeeUuid";

@implementation QAttendeeCode

- (NSDictionary *) serialize {
    
    NSMutableDictionary *dictionary = [@{
                                        @"type": self.type,
                                        @"code": self.code
                                        } mutableCopy];
    
    if(self.firstname){
        [dictionary setObject:self.firstname forKey:@"firstname"];
    }
    
    if(self.lastname){
        [dictionary setObject:self.lastname forKey:@"lastname"];
    }
    
    return dictionary;
    
}

@end

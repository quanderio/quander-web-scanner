//
//  QUDiscoveredCodeOverlayView.swift
//  Quander
//
//  Created by Sean Howard on 12/06/2016.
//  Copyright © 2016 Quander Ltd. All rights reserved.
//

import UIKit

class DiscoveredCodeOverlayView: UIView {
    let borderLayer = CAShapeLayer()
    var corners : [CGPoint]?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func drawBorder(points : [CGPoint]) {
        self.corners = points
        
        let path = UIBezierPath()
        path.move(to: points.first!)
        
        for i in 1 ..< points.count {
            path.addLine(to: points[i])
        }
        
        path.addLine(to: points.first!)
        borderLayer.path = path.cgPath
    }
    
    func setupView() {
        borderLayer.strokeColor = UIColor(red:0.31, green:1.00, blue:0.85, alpha:1.0).cgColor
        borderLayer.lineWidth = 5.0
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.lineCap = kCALineCapRound
        borderLayer.lineJoin = kCALineJoinRound
        self.layer.addSublayer(borderLayer)
    }
}

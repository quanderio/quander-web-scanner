//
//  QUScanner.swift
//  Quander
//
//  Created by Sean Howard on 12/06/2016.
//  Copyright © 2016 Quander Ltd. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox

enum QUScannerSymbology {
    case QR
    case Barcode
}

protocol QUScannerDelegate : class {
    func scanner(scanner : QUScanner, didReadString string : String)
    func scanner(scanner : QUScanner, setupDidFailWithError error : Error)
}

class QUScanner : NSObject, AVCaptureMetadataOutputObjectsDelegate {
    
    private let captureSession = AVCaptureSession()
    private var previewLayer : AVCaptureVideoPreviewLayer?
    private var captureDevice : AVCaptureDevice?
    private var identifiedBorder : DiscoveredCodeOverlayView?
    
    private var hideBorderCommand : dispatch_cancelable_closure?
    
    private var delayActive : Bool = false {
        didSet {
            if delayActive && shouldDelay {
                
                _ = delay(time: self.delayTime, closure: {
                    self.delayActive = false
                });
            }
        }
    }
    
    /// The UIView to display the camera feed
    let scannerView : UIView
    /// Get feedback when the scanner reads something
    let delegate : QUScannerDelegate?
    /// Set whether the scanner should read barcodes or QR codes.
    let scannerSymbology : QUScannerSymbology
    /// If paused the class won't callback when a code is read.
    var paused : Bool = false
    /// Inidicates if there should be a delay time between readings.
    var shouldDelay : Bool = true
    /// The delay time between readings if ```shouldDelay``` is ```true```. Default time is 2 seconds
    var delayTime : Double = 2.0
    /// Inidicates the current status of the torch
    var torchOn : Bool {
        
        guard let captureDevice = self.captureDevice else {
            return false
        }
        
        switch captureDevice.torchMode {
        case .on, .auto:
            return true
        case .off:
            return false
        }
    }
    var vibrationFeedback : Bool = false
    var audioFeedback : Bool = false
    
    /**
     Initialises the class with all the required components for setup.
     
     - parameter view:        The ```UIView``` to display the camera feed
     - parameter symbology:   Set whether the scanner should read barcodes or QR codes
     - parameter shouldDelay: Should there be a delay between readings or continuous readings.
     - parameter delayTime:   Delay time between readings, defaults to 2 seconds
     - parameter delegate:    Get feedback when the scanner reads something
     
     - returns: Initialisation of ```JSScanner``` class.
     */
    init(withView view : UIView, symbology : QUScannerSymbology, shouldAutomaticallyDelayBetweenReadings shouldDelay : Bool, audioFeedback : Bool, vibrationFeedback : Bool, delayTimeBetweenReadings delayTime : Double?, delegate : QUScannerDelegate?) {
        self.scannerView = view
        self.scannerSymbology = symbology
        self.delegate = delegate
        self.shouldDelay = shouldDelay
        self.vibrationFeedback = vibrationFeedback
        self.audioFeedback = audioFeedback
        
        if let delayTime = delayTime {
            self.delayTime = delayTime
        }
        
        super.init()
        
        #if (arch(i386) || arch(x86_64))
            print("Camera unavailable, will not attempt to setup live camera feed")
        #else
            self.setupCaptureDeviceInput()
        #endif
    }
    
    private func setupCaptureDeviceInput() {
        
        let deviceDiscoverySession = AVCaptureDeviceDiscoverySession(deviceTypes: [.builtInDuoCamera, .builtInTelephotoCamera, .builtInWideAngleCamera], mediaType: AVMediaTypeVideo, position: .front)
        
        guard let captureDevice : AVCaptureDevice = deviceDiscoverySession?.devices.first
 else {
            return
        }
        
        self.captureDevice = captureDevice
        
        do {
            
            try captureDevice.lockForConfiguration()
            
            if self.captureDevice!.isFocusModeSupported(.continuousAutoFocus) {
                self.captureDevice?.focusMode = .continuousAutoFocus
            }
            
            if self.captureDevice!.isWhiteBalanceModeSupported(.continuousAutoWhiteBalance) {
                self.captureDevice?.whiteBalanceMode = .continuousAutoWhiteBalance
            }
            
            if self.captureDevice!.isExposureModeSupported(.continuousAutoExposure) {
                self.captureDevice?.exposureMode = .continuousAutoExposure
            }
            
            captureDevice.unlockForConfiguration()
            
        } catch (let error) {
            self.delegate?.scanner(scanner: self, setupDidFailWithError: error)
        }
        
        do {
            let inputDevice = try AVCaptureDeviceInput(device: captureDevice)
            captureSession.addInput(inputDevice)
            
            self.addCapturePreviewLayer()
            
            self.setupCaptureOutput()
            
        } catch (let error) {
            self.delegate?.scanner(scanner: self, setupDidFailWithError: error)
        }
    }
    
    private func addCapturePreviewLayer() {
        
        self.previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
        self.previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.previewLayer?.frame = self.scannerView.layer.bounds
        
        self.scannerView.layer.addSublayer(self.previewLayer!)
        
        identifiedBorder = DiscoveredCodeOverlayView(frame : self.scannerView.bounds)
        identifiedBorder?.backgroundColor = UIColor.clear
        identifiedBorder?.isHidden = true
        self.scannerView.addSubview(identifiedBorder!)
    }
    
    private func setupCaptureOutput() {
        let output = AVCaptureMetadataOutput()
        self.captureSession.addOutput(output)
        
        var metaDataObjectTypes = [String]()
        
        /*
         Not available as a AVMetadataObjectType in AVFoundation.
         [settings setSymbology:SBSSymbologyUPC12 enabled:YES];
         */
        switch self.scannerSymbology {
        case .QR:
            metaDataObjectTypes = [
                AVMetadataObjectTypeQRCode
            ]
            
            break;
        case .Barcode:
            metaDataObjectTypes = [
                AVMetadataObjectTypeEAN8Code,
                AVMetadataObjectTypeEAN13Code,
                AVMetadataObjectTypeUPCECode,
                AVMetadataObjectTypeCode39Code,
                AVMetadataObjectTypeCode93Code,
                AVMetadataObjectTypeCode128Code
            ]
            break;
            
        }
        
        output.metadataObjectTypes = metaDataObjectTypes
        
        let metaDataOutputQueue = DispatchQueue(label: "com.quander.QUMetaDataOutputQueue")
        output.setMetadataObjectsDelegate(self, queue: metaDataOutputQueue)
    }
    
    /**
     Updates the camera preview layer to fit the frame of the ```scannerView```.
     
     This should be called on ```viewDidLayoutSubviews()``` and when there's any orientation changes.
     */
    func updateFrames() {
        let orientation: UIDeviceOrientation = UIDevice.current.orientation
        
        self.previewLayer?.frame = self.scannerView.bounds;
        
        switch (orientation) {
        case .portrait:
            previewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.portrait
            break
        case .landscapeRight:
            previewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.landscapeLeft
            break
        case .landscapeLeft:
            previewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.landscapeRight
            break
        default:
            previewLayer?.connection.videoOrientation = AVCaptureVideoOrientation.portrait
            break
        }
    }
    
    /**
     Start running the camera feed and start reading for codes.
     */
    func start() {
        paused = false
        self.captureSession.startRunning()
    }
    
    /**
     Stop running the camera feed and stop reading for codes.
     */
    func stop() {
        paused = true
        self.captureSession.stopRunning()
    }
    
    /**
     Resume reading for codes again.
     */
    func resume() {
        paused = false
    }
    
    /**
     Pause reading for codes but continue running camera feed.
     */
    func pause() {
        paused = true
    }
    
    /**
     Toggles the torch on and off
     */
    func toggleTorch() {
        guard let captureDevice = self.captureDevice , captureDevice.isTorchAvailable && captureDevice.hasTorch else {
            return
        }
        
        if captureDevice.isTorchModeSupported(.off) && captureDevice.isTorchModeSupported(.on) {
            
            do {
                try captureDevice.lockForConfiguration()
                captureDevice.torchMode = (torchOn) ? .off : .on
                captureDevice.unlockForConfiguration()
            } catch (let error) {
                print(error)
            }
        }
    }
    
    //MARK:- AVCaptureMetadataOutputObjectsDelegate
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        if paused {
            return
        }
        
        guard let dataObject = metadataObjects.first else {
            return
        }
        
        let metaDataObject = dataObject as! AVMetadataObject
        
        guard let transformed = self.previewLayer?.transformedMetadataObject(for: metaDataObject) as? AVMetadataMachineReadableCodeObject,
            let stringValue = transformed.stringValue else {
                return
        }
        
        DispatchQueue.main.async {
            self.identifiedBorder?.frame = transformed.bounds
            
            let identifiedCorners = self.translatePoints(points: transformed.corners as [AnyObject], fromView: self.scannerView, toView: self.identifiedBorder!)
            self.identifiedBorder?.drawBorder(points: identifiedCorners)
            self.identifiedBorder?.isHidden = false
            
            if let hideBorderCommand = self.hideBorderCommand {
                cancel_delay(closure: hideBorderCommand)
            }
            
            self.hideBorderCommand = delay(time: 0.05) {
                self.identifiedBorder?.isHidden = true
            }
 
        }
        
        if delayActive {
            return
        }
        
        if shouldDelay {
            delayActive = true
        }
        
        
        DispatchQueue.main.async {
            self.delegate?.scanner(scanner: self, didReadString: stringValue)
        }

        if self.audioFeedback {
            self.playAudioFeedback()
        }
        
        if self.vibrationFeedback {
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
        }
    }
    
    func removeBorder() {
        self.identifiedBorder?.isHidden = true
    }
    
    private func translatePoints(points : [AnyObject], fromView : UIView, toView: UIView) -> [CGPoint] {
        var translatedPoints : [CGPoint] = []
        for point in points {
            let dict = point as! NSDictionary
            
            let x : CGFloat = dict["X"] as! CGFloat
            let y : CGFloat = dict["Y"] as! CGFloat
            
            let curr = CGPoint(x: x, y: y)
            let currFinal = fromView.convert(curr, to: toView)
            translatedPoints.append(currFinal)
        }
        return translatedPoints
    }
    
    
    private func playAudioFeedback() {
        let path = Bundle.main.path(forResource: "beep", ofType: "wav")
        let url = NSURL(fileURLWithPath: path!)
        var soundID = SystemSoundID()
        
        AudioServicesCreateSystemSoundID (url as CFURL, &soundID);
        AudioServicesPlaySystemSound(soundID);
    }
}

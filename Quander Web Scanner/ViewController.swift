//
//  ViewController.swift
//  Show Portrait Controller
//
//  Created by Gavin Williams on 29/11/2016.
//  Copyright © 2016 Gavin Williams. All rights reserved.
//

import UIKit
import AVFoundation
import WebViewJavascriptBridge

class ViewController: UIViewController, QUScannerDelegate, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var scannerView: UIView!
    @IBOutlet weak var playerView: UIView!
    
    var bridge: WebViewJavascriptBridge!
    var scanner: QUScanner!
    var playerLayer: AVPlayerLayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupScanner()
        setupPlayer()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setupWebView()
        setupBridge()
    }
    
    func setupScanner(){
        
        scanner = QUScanner(withView: scannerView, symbology: .QR, shouldAutomaticallyDelayBetweenReadings: true, audioFeedback: true, vibrationFeedback: true, delayTimeBetweenReadings: 3.0, delegate: self)
        self.scannerView.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        scanner.updateFrames()
        playerLayer.frame = playerView.bounds
    }
    
    func setupPlayer() {
        
        playerLayer = AVPlayerLayer()
        playerView.layer.addSublayer(playerLayer)
        
    }
    
    func setupBridge() {
        
        bridge = WebViewJavascriptBridge(for: webView)
        
        bridge.registerHandler("stopScanner", handler: { (data, responseCallback) in
            self.scannerView.isHidden = true
            self.scanner.stop()
        })
        
        bridge.registerHandler("startScanner", handler: { (data, responseCallback) in
            self.scannerView.isHidden = false
            self.scanner.start()
        })
        
        bridge.registerHandler("resumeScanner", handler: { (data, responseCallback) in
            self.scanner.resume()
        })
        
        bridge.registerHandler("pauseScanner", handler: { (data, responseCallback) in
            self.scanner.pause()
        })
        
        bridge.registerHandler("playVideo", handler: { (data, reponseCallback) in
            
            guard let data = data as? Dictionary<String, AnyObject> else {
                return
            }
            
            guard let video = data["video"] as? String else {
                return
            }
            
            guard let file = Bundle.main.url(forResource: video, withExtension: nil) else {
                return
            }
            
            let player = AVPlayer(url: file)
            
            self.playerLayer.player = player
            
            self.playerView.isHidden = false
            
            player.play()
            
        })
        
        bridge.registerHandler("stopVideo", handler: { (data, responseCallback) in
            
            self.playerView.isHidden = true
            
            if let player = self.playerLayer.player {
                
                player.pause()
                self.playerLayer.player = nil
                
            }
        
        })
    
    }
    
    func setupWebView(){
        
        webView.mediaPlaybackRequiresUserAction = false
        webView.allowsInlineMediaPlayback = true
        webView.delegate = self
        
        webView.scrollView.bounces = false

        webView.isOpaque = false
        webView.backgroundColor = UIColor.clear
        
        self.loadWebApp()

    }
    
    func loadWebApp(){
        guard let webapp_url = UserDefaults.standard.string(forKey: "webapp_url"), let url = URL(string: webapp_url) else {
            self.handleNoWebAppError()
            return
        }
        
        let request = URLRequest(url: url)
        webView.loadRequest(request)
    }
    
    func handleNoWebAppError(){
        
        let alert: UIAlertController = UIAlertController(title: "Invalid Web App URL", message: "You must set a webapp in the settings app", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Open Preferences", style: .default, handler: { (_) in
            
            guard let settings = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settings) {
                UIApplication.shared.open(settings, options: [:], completionHandler: nil)
            }
            
        })
        
        alert.addAction(action)
        
        present(alert, animated: true, completion: nil)
    }

    func scanner(scanner: QUScanner, didReadString string: String) {
        
        guard let code: QAttendeeCode = QCode.code(from: string) else {
            bridge.callHandler("scannerError", data: [
                "code": 1,
                "description": "Could not recognise QR code",
                "resoloution": "Please scan another QR code"
            ])
            scanner.resume()
            return
        }
        
        bridge.callHandler("scannerReceivedAttendee", data: [
                "firstname": code.firstname,
                "lastname": code.lastname,
                "type": code.type,
                "code": code.code
        ])
        
    }
    
    func scanner(scanner: QUScanner, setupDidFailWithError error: Error) {
        bridge.callHandler("scannerError", data: [
            "code": 2,
            "description": "Could not start scanner",
            "resoloution": "Please ensure that the app has permission to use the camera"
        ])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
        let alert: UIAlertController = UIAlertController(title: "Load Failed", message: error.localizedDescription, preferredStyle: .alert)
        
        let yes = UIAlertAction(title: "Reload", style: .default, handler: {(_) in
            self.loadWebApp()
        })
        
        let no = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(yes);
        alert.addAction(no);
        
        present(alert, animated: true, completion: nil)
        
    }


}

